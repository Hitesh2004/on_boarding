@extends("admin.layouts.app")

@section('main-content')
    <div class="container">
        <div class="card">
            <div class="d-flex align-items-end justify-content-between">
                <div class="d-flex align-items-center justify-content-between bg-white py-2 px-4 mr2 border">
                    <div class="mb-3">
                        <input type="text" id="search-name" class="form-control search-name" name="name" placeholder="Name">
                    </div>
                    <div class="mb-3">
                        <input type="text" id="search-description" class="form-control search-fields" name="description" placeholder="Description">
                    </div>
                    <div class="mb-3">
                        <input type="text" id="search-price" class="form-control search-price" name="name" placeholder="Price">
                    </div>
                    <div class="mb-3">
                        <input type="text" id="search-stocks" class="form-control search-fields" name="description" placeholder="Stock">
                    </div>
                    <button class="btn btn-info" data-bs-toggle="modal" data-bs-target="#export-modal" id="export-modal-button">Export</button>

                </div>

                <a href="{{route("admin.products.create")}}" class="btn btn-success">Create Product</a>


                <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
                    Import
                </button>

                <!-- Modal -->
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                ...
                            </div>
                            <div class="modal-footer">
                                <form action="{{ route("admin.products.import") }}" enctype="multipart/form-data" method="POST">
                                    @csrf
                                    @method("POST")
                                    <input type = "file" name="file">

                                    <button type="submit" class="btn btn-info">Import Excel</button>
                                </form>

                                <a href="{{route("admin.products.saveUpLoadForm")}}" class="btn btn-success">Download Sample File</a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="card-header">Products</div>

            <div class="card-body">

                {!! $dataTable->table(['id' => 'products-table']) !!}
            </div>
        </div>
    </div>

    <div class="modal modal-blur fade" id="export-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-sm modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="modal-title">Export</div>
                    <div>Export the displayed data?</div>
                </div>
                <div class="modal-footer">
                    <form action="{{route('admin.products.export')}}" method="POST">
                        @method('POST')
                        @csrf
                        <input type="text" hidden id="filtered-data" name="filtered-data" >
                        <button type="button" class="btn btn-link link-secondary me-auto" data-bs-dismiss="modal">Cancel</button>
                        <input type="submit" class="btn btn-primary" value="Export Excel">
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-name')
    products
@endsection

@section("scripts")
    <script src="{{asset("vendor/jquery/jquery.min.js")}}"></script>
    <script src="{{ asset("vendor/datatables/jquery.dataTables.min.js") }}"></script>
    <script src="{{asset('frontend/dist/js/dataTables.js')}}"></script>
    <script src="{{ asset("vendor/datatables/dataTables.bootstrap4.min.js") }}"></script>

    {{ $dataTable->scripts() }}

    <script>
        $(document).ready(function() {
            const filteredDataInput = document.querySelector('#filtered-data')
            const btn = document.querySelector("#export-modal-button")
            btn.addEventListener('click', () => {
                var data = table.column(0).data().toArray();
                console.log(data)
                filteredDataInput.value = data
            })
            // DataTable initialization
            var table = $('#products-table').DataTable();

            // Apply the search
            $('#search-name').on('keyup change', function () {
                table.page.len(-1).draw();
                table.column(2).search(this.value).draw();
            });
            $('#search-description').on('keyup change', function () {
                table.page.len(-1).draw();
                table.column(3).search(this.value).draw();
            });
            $('#search-price').on('keyup change', function () {
                table.page.len(-1).draw();
                table.column(4).search(this.value).draw();
            });
            $('#search-stocks').on('keyup change', function () {
                table.page.len(-1).draw();
                table.column(5).search(this.value).draw();
            });

        });
    </script>
@endsection

@section("styles")
    <link href="{{asset("vendor/datatables/dataTables.bootstrap4.min.css")}}" rel="stylesheet">
@endsection
