@extends("admin.layouts.app")

@section('main-content')

    <div class="row">
        <div class="col-md-12">
            <form action="{{route("admin.categories.update", $category)}}" method="POST">
                @csrf
                @method("PUT")
                <h1 class="ms-8 mt-6"> Create Category</h1>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="=name" class="ms-8 mt-4">Name</label>
                        <input type="text" class="form-control ms-8 mt-2" name="name" id="=name" value="{{ $category->name }}">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="=description" class="ms-8 mt-4">Description</label>
                        <input type="text" class="form-control ms-8 mt-2" name="description" id="=description" value="{{ $category->description }}">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="state" class="ms-8 mt-4">Status</label>
                        <select type="text" class="form-control ms-8 mt-2" name="state" id="state">
                            <option value="1"  {{ $category->is_active ? 'selected' : '' }}>Active</option>
                            <option value="0"  {{ !$category->is_active ? 'selected' : '' }}>Inactive</option>
                        </select>
                    </div>



                    <div class="form-group col-md-12 d-flex justify-content-end">
                        <input type="submit" class="btn btn-primary me-auto ms-8 mt-4" value="Update">
                    </div>

                </div>
                <form method="POST" action="{{route("admin.categories.destroy", $category->id)}}"></form>
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-danger me-8 ms-8 mt-4">Delete</button>

            </form>
        </div>
    </div>
@endsection


