@extends('admin.layouts.app')

@section('main-content')
    <div class="container-xl">
        {{--        <div class="row row-cards">--}}
        <div class="col-lg-6">
            <form class="card" action="{{route('admin.categories.store')}}" method="POST" id="category-form">
                @csrf
                <div class="card-body">
                    <h2 class="">Create Category</h2>
                    <div class="row row-cards" id="category-list">
                        <div class="single-category">
                            <div class="col-md-6">
                                <div class="mb-3">
                                </div>
                            </div>
                            <div class="col-md-6 ">
                                <div class="mb-3">
                                    <label class="form-label">Category Name</label>
                                    <input type="text" class="form-control name" placeholder="Category Name" name="name[]">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="mb-3 mb-0">
                                    <label class="form-label">Category Description</label>
                                    <textarea name="description[]" rows="5" class="form-control description" placeholder="Some details about the category."></textarea>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="mb-3">
                                    <label class="form-label">Status</label>
                                    <select class="form-control form-select" name="state[]">
                                        <option value="1">Active</option>
                                        <option value="0">Inactive</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
{{--                    <div class="col-3">--}}
{{--                        <button class="btn" id="addCategory" >+ Add</button>--}}
{{--                    </div>--}}
                </div>
                <div class="card-footer text-start">
                    <input type="submit" class="btn btn-primary" value="Create">
                </div>
            </form>
        </div>
        {{--        </div>--}}
    </div>
@endsection

@section('scripts')
    <script>
//         document.addEventListener('DOMContentLoaded', function() {
//             const btn = document.querySelector("#addCategory");
//             const categoryList = document.querySelector("#category-list");
//
//             const singleCategory = `<div class="single-category">
//                 <div class="col-md-6">
//                     <div class="mb-3">
//
//         </div>
//     </div>
//     <div class="col-md-6 ">
//         <div class="mb-3">
//             <label class="form-label">Category Name</label>
//             <input type="text" class="form-control" placeholder="Category Name" name="name[]">
//         </div>
//     </div>
//     <div class="col-md-12">
//         <div class="mb-3 mb-0">
//             <label class="form-label">CAtegory Description</label>
//             <textarea name="description[]" rows="5" class="form-control" placeholder="Some details about the category."></textarea>
//         </div>
//     </div>
//
//     <div class="col-md-12">
//         <div class="mb-3">
//             <label class="form-label">Status</label>
//             <select class="form-control form-select" name="state[]">
//                 <option value="1">Active</option>
//                 <option value="0">Inactive</option>
//             </select>
//         </div>
//     </div>
// </div>`;
//
//             btn.addEventListener('click', function(evt) {
//                 evt.preventDefault();
//                 categoryList.insertAdjacentHTML('beforeend', singleCategory);
//             });
//         });
    </script>

    <script src="{{asset('jquery/jquery.min.js')}}"></script>
    <script src="{{asset('jquery-validation/dist/jquery.validate.min.js')}}"></script>
    <script src="{{asset('jquery-validation/dist/additional-method.js')}}"></script>

    <script>
        jQuery.validator.addClassRules({
            name: {
                required:true,
                minlength: 3,
                maxlength: 255

            },

            description: {
                required:true,
                minlength: 3,
                maxlength: 255

            }

        });

        $('#category-form').validate();
    </script>
@endsection


