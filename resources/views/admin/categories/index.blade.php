@extends("admin.layouts.app")

@section('main-content')
    <div class="container">
        <div class="card">
            <div class="d-flex align-items-end justify-content-between">
                <div class="d-flex align-items-center justify-content-between bg-white py-2 px-4 mr2 border">
                    <div class="mb-3">
                        <input type="text" id="search-name" class="form-control search-name" name="name" placeholder="Name">
                    </div>
                    <div class="mb-3">
                        <input type="text" id="search-description" class="form-control search-fields" name="description" placeholder="Description">
                    </div>
                    <button class="btn btn-info" data-bs-toggle="modal" data-bs-target="#export-modal" id="export-modal-button">Export</button>

                </div>

                <a href="{{route("admin.categories.create")}}" class="btn btn-success">Create category</a>


                <div class="d-flex align-items-center justify-content-between bg-white py-2 px-4 mr2 border">
                    <form action="{{route("admin.categories.import")}}" enctype="multipart/form-data" method="POST">
                        @csrf
                        <input type="file" name="file">
                        <button type="submit" class="btn btn-info">Import Excel</button>
                    </form>
                </div>

            </div>

            <div class="card-header">Category</div>

            <div class="card-body">

                {!! $dataTable->table(['id' => 'categories-table']) !!}
            </div>
        </div>
    </div>

    <div class="modal modal-blur fade" id="export-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-sm modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="modal-title">Export</div>
                    <div>Export the displayed data?</div>
                </div>
                <div class="modal-footer">
                    <form action="{{route('admin.categories.export')}}" method="POST">
                        @method('POST')
                        @csrf
                        <input type="text" hidden id="filtered-data" name="filtered-data">
                        <button type="button" class="btn btn-link link-secondary me-auto" data-bs-dismiss="modal">Cancel</button>
                        <input type="submit" class="btn btn-primary" value="Export Excel">
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-name')
    Categories
@endsection

@section("scripts")
    <script src="{{asset("vendor/jquery/jquery.min.js")}}"></script>
    <script src="{{ asset("vendor/datatables/jquery.dataTables.min.js") }}"></script>
    <script src="{{asset('frontend/dist/js/dataTables.js')}}"></script>
    <script src="{{ asset("vendor/datatables/dataTables.bootstrap4.min.js") }}"></script>

    {{ $dataTable->scripts() }}

    <script>
        $(document).ready(function() {
            const filteredDataInput = document.querySelector('#filtered-data')
             const btn = document.querySelector("#export-modal-button")
            btn.addEventListener('click', () => {
                var data = table.column(0).data().toArray();
                console.log(data)
                filteredDataInput.value = data
            })
            // DataTable initialization
            var table = $('#categories-table').DataTable();

            // Apply the search
            $('#search-name').on('keyup change', function () {
                table.page.len(-1).draw();
                table.column(1).search(this.value).draw();
            });
            $('#search-description').on('keyup change', function () {
                table.page.len(-1).draw();
                table.column(2).search(this.value).draw();
            });
        });
    </script>
@endsection

@section("styles")
    <link href="{{asset("vendor/datatables/dataTables.bootstrap4.min.css")}}" rel="stylesheet">
@endsection
