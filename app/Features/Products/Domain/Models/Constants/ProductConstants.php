<?php

namespace App\Features\Products\Domain\Models\Constants;

interface ProductConstants
{
    public const UPDATE_VALIDATION_RULES = [
        "category_id" => "required|exists:categories,id",
        "name" => "required|string|max:255",
        "description" => "required|nullable|string",
        "price" => "required|numeric",
        "stock" => "required|integer",
        "is_active" => "required|boolean",
    ];

    public const PERSIST_VALIDATION_RULES = [
        "category_id" => "required|exists:categories,id",
        "name" => "required|string|max:255",
        "description" => "required|nullable|string",
        "price" => "required|numeric",
        "stock" => "required|integer",
        "is_active" => "required|boolean",
    ];
}
