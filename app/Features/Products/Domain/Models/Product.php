<?php

namespace App\Features\Products\Domain\Models;

use App\Features\Categories\Domain\Models\Category;
use App\Features\Orders\Domain\Models\Order;
use App\Features\Products\Domain\Models\Constants\ProductConstants;
use App\Helpers\Models\BaseModel;
use App\Helpers\Services\Utils;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Product extends BaseModel implements ProductConstants
{
    protected $table = 'products';





    public static function persistRules(): array
    {
        return self::PERSIST_VALIDATION_RULES;
    }

    public static function updateRules(): array
    {
        return self::UPDATE_VALIDATION_RULES;
    }





}
