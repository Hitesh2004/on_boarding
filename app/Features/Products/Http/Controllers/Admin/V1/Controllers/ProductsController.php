<?php

namespace App\Features\Products\Http\Controllers\Admin\V1\Controllers;

use App\DataTables\ProductsDataTable;
use App\Features\Categories\Domain\Imports\CategoriesImport;
use App\Features\Categories\Domain\Models\Category;
use App\Features\Products\Domain\Export\ProductsExport;
use App\Features\Products\Domain\Imports\ProductsImport;
use App\Features\Products\Domain\Models\Product;
use App\Features\Products\Http\Controllers\Admin\V1\Actions\ProductsAction;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateProductRequest;
use App\Http\Requests\UpdateProductRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;

class ProductsController extends Controller
{

    public function index()
    {
        $products = Product::all();
        return view("admin.products.index", compact('products'));
    }

}
