<?php

namespace App\Features\Categories\Http\Controllers\Admin\V1\Controllers;

use App\DataTables\CategoriesDataTable;
use App\Features\Categories\Domain\Exports\CategoriesExport;
use App\Features\Categories\Domain\Imports\CategoriesImport;
use App\Features\Categories\Domain\Models\Category;
use App\Features\Categories\Http\Controllers\Admin\V1\Actions\CategoriesAction;
use App\Features\Products\Domain\Models\Product;
use App\Features\Products\Http\Controllers\Admin\V1\Actions\ProductsAction;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateCategoryRequest;
use App\Http\Requests\UpdateCategoryRequest;
use \Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;
use mysql_xdevapi\CollectionModify;
use function Laravel\Prompts\table;
use function Termwind\render;


class CategoriesController extends Controller
{

    public function __construct(private CategoriesAction $categoriesAction, private ProductsAction $productsAction){}

    public function index(CategoriesDataTable $dataTable) {
//        dd("Hello");
        return $dataTable->render('admin.categories.index');
    }

    public function create(Category $category, Request $request)
    {
        return view("admin.categories.create");
    }

    public function edit(Category $category, Request $request)
    {
        return view("admin.categories.edit", compact(["category"]));

    }

    public function delete(Category $category, Request $request)
    {
        return view("admin.categories.delete", compact(["category"]));

    }

//    public function show()
//    {
//        return view("admin.categories.add_more");
//    }


    public function update(Category $category, UpdateCategoryRequest $request)
    {
        try{
            $data["name"] = $request->name;
            $data["description"] = $request->description;
            $data["is_active"] = $request->state === "1";
            $this->categoriesAction->updateCategory($category, $data);
            $category->update(['updated_at' => now()]);
            session()->flash("success", "Category details saved successfully!");
        } catch (\Exception $exception) {
            Log::info($exception);
            session()->flash("error", "Error while saving details!");
        }
        return redirect()->route("admin.categories.index");

    }

//


    public function store(CreateCategoryRequest $request)
    {
        try{


            for ($i=0; $i < count($request->name); $i++ ) {
                $data["name"] = $request->name[$i];
                $data["description"] = $request->description[$i];
                $data["is_active"] = $request->state[$i];
//                dd($data);
                $this->categoriesAction->persistCategory($data);

            }
            session()->flash("success", "Category details saved successfully!");
        } catch (\Exception $exception) {
            Log::info($exception);
            session()->flash("error", "Error while saving details!");
        }
        return redirect()->route("admin.categories.index");
    }
////


    public function destroy($id)
    {
        Category::findOrFail($id)->delete();
        return redirect()->route("admin.categories.index");
    }

    public function show()
    {

    }





}
