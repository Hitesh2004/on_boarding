<?php
namespace App\Features\Categories\Http\Controllers\Admin\V1\Actions;
use App\Features\Categories\Domain\Imports\CategoriesImport;
use App\Features\Categories\Domain\Models\Category;
use Maatwebsite\Excel\Facades\Excel;

class  CategoriesAction
{
    public function persistCategory(array $data) : Category
    {
        return Category::persistCategory($data);
    }

    public function updateCategory(Category $category, array $data) : Category
    {
        return $category->updateCategory($data);

    }




}
