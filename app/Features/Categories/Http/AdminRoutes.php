<?php
use Illuminate\Support\Facades\Route;

Route::resource(
    "/categories",\App\Features\Categories\Http\Controllers\Admin\V1\Controllers\CategoriesController::class
)->except(['show']);



Route::resource(
    "/products",\App\Features\Products\Http\Controllers\Admin\V1\Controllers\ProductsController::class
)->except(['show']);
