<?php

namespace App\Features\Categories\Domain\Models\Constants;

interface CategoryConstants
{
    public const PERSIST_VALIDATION_RULES = [
        "name" => "required|max:255",
        "description" => "required",
        "is_active" => "required",
    ];

    public const UPDATE_VALIDATION_RULES = [
        "name" => "required|max:255",
        "description" => "required",
        "is_active" => "required",
    ];

}
