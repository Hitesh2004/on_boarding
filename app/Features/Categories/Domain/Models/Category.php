<?php

namespace App\Features\Categories\Domain\Models;

use App\Features\Categories\Domain\Models\Constants\CategoryConstants;
use App\Features\Products\Domain\Models\Product;
use App\Helpers\Models\BaseModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Helpers\Services\Utils;

class Category extends BaseModel implements  CategoryConstants
{
    public $timestamps = true;
    public mixed $ProductAction;

    protected $table = 'categories';
    public static function persistCategory(array $data): self {
//        validation

        Utils::validateOrThrow($data, self::persistRules());
        return DB::transaction(function() use($data) {
            return Category::create($data);
        });
    }


    public function updateCategory(array $data): Category
    {
        Utils::validateOrThrow($data, self::updateRules());
        DB::transaction(function () use ($data) {
            $this->update($data);
        });
        return $this;
    }

        public static function persistRules(): array
    {
        return self::PERSIST_VALIDATION_RULES;
    }

    public static function updateRules(): array
    {
        return self::UPDATE_VALIDATION_RULES;
    }




}

